var map = L.map('map').setView([20.25, 85.8], 13);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 15,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

/* Marker */
var marker = L.marker([20.25, 85.8]).addTo(map);

/* Circle */
var circle = L.circle([20.25, 85.8], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(map);

/* Polygon */
var polygon = L.polygon([
    [20.23, 85.7],
    [20.20, 85.75],
    [20.21, 85.72]
]).addTo(map);

/* Popup */
marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
circle.bindPopup("I am a circle.");
polygon.bindPopup("I am a polygon.").openPopup();

/* A standalone popup */
var popup = L.popup()
    .setLatLng([51.513, -0.09])
    .setContent("I am a standalone popup.")
    .openOn(map);

/* Events */    
function onMapClick(e) {
    alert("You clicked the map at " + e.latlng);
}
map.on('click', onMapClick);